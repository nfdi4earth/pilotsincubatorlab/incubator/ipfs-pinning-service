# How to Use the IPFS Pinning Service for Open Climate Research Data
An IPFS pinning service is a type of service that allows users to store and maintain the availability of their files on the IPFS network. The goal of an IPFS pinning service is to provide a reliable and trusted way for users to ensure that their files remain accessible on the IPFS network. This is accomplished by maintaining a copy of the file on the service's own storage infrastructure, which is then pinned to the IPFS network. This allows users to access the file even if the original source becomes unavailable.

The researcher, who wants to use the pinning service and wants to access data, needs to follow the client side instructions. The institution, which wants to implement a data hosting and pinning service, needs to follow the provider side instructions.

## Client Side (Researcher)
- The researcher opens the IPFS Desktop App.
<img src="https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/raw/main/Documents/Screenshots/desktop_app_traffic.png?inline=false" alt="traffic"/>

- The researcher clicks `Import` -> `File`
<img src="https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/raw/main/Documents/Screenshots/desktop_app_files.png?inline=falsee" alt="files"/>

- Once the file has been uploaded to the IPFS, the researcher can copy the unique content identifier `Copy CID` and share it with the institution where the data shall be pinned.
<img src="https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/raw/main/Documents/Screenshots/copy_CID.png?inline=false" alt="cid" width="200"/>

An external tutorial on the full functionality of the IPFS Desktop App can be found here: [IPFS Desktop Tutorial](https://curriculum.pl-launchpad.io/tutorials/ipfs-intro/desktop-tutorial/)

## Provider Side (Institution where Data can be pinned)
The service provider receives the CID via a shared repository where the researchers indicate which data CIDs they want to pin.
Once the pinning service provider has received the CID, the data can be pinned in the following steps:
- One time, the IPFS command line application must be started on the cloud instance/virtual machine by typing `ipfs daemon`.
- Then, each time a file needs to be pinned, the provider enters: `ipfs pin add <CID>` (this can be fully automated)

This is a minimal example of the workflow. For a more comprehensive overview of the command line options check out https://docs.ipfs.tech/install/command-line/ or go through the [IPFS Command Line Tutorial](https://curriculum.pl-launchpad.io/tutorials/ipfs-intro/basics/)


## Data Access
Once the data is pinned, it is available from everywhere and anytime. For example a Zarr file in the IPFS can simply be accessed by the xArray Python library: `xr.open_zarr("ipfs://<CID>)`.

An example of the workflow including the data access can be seen in the application tutorial [video](https://youtu.be/jDrBFgUi7Oc) and the corresponding [notebook](https://gitlab.dkrz.de/data-infrastructure-services/ipfs-pinning-service-for-open-climate-research-data/-/blob/main/Notebook/how_to_open_ipfs_file.ipynb).
