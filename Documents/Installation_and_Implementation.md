# Installation of the IPFS

## What is the IPFS
"**IPFS** (InterPlanetary File System) is a distributed system for storing and accessing files, websites, applications, and data." [https://docs.ipfs.tech/concepts/what-is-ipfs/]

## How does the IPFS support FAIR Data Workflows?
- **Findable:** In the IPFS each data block has a unique content identifier (CID). These content identifiers can easily included in existing data catalogues in order to make the data findable
- **Accessible:** IPFS data can be accessed from anywhere and anytime without authentication or authorization. This makes it a truly open network.
- **Interoperable:** Python libraries, such es xarray can access data from the IPFS. This makes it easy to interoperate IPFS data within existing work flows.
- **Reproducible:** Data within the IPFS is immutable. This ensures the reproducibility of workflows over a long time period.


## Installation and Implementation of an IPFS Node
An IPFS node needs to be installed and implemented by all involved parties. The researcher, who wants to publish and access data, needs to follow the client side instructions. The institution, which wants to implement a data hosting and pinning service, needs to follow the provider side instructions.

### Client Side (Researcher)
The installation is available for Windows, Mac, Ubuntu. The IPFS desktop app can be installed via https://docs.ipfs.tech/install/ipfs-desktop/. The application is ready to use for the the researcher without further configuration.

### Provider Side (Institution where Data can be pinned)
In our study a cloud instance/virtual machine has been employed with the following parameters:
- CentOS8Stream OS
- 2 VCPUs
- 2 GB RAM
- 10 GB + 20 GB additional volume disk storage

The installation binaries for various operating systems can be downloaded via: https://docs.ipfs.tech/install/command-line/#install-official-binary-distributions. In our case we used the `kubo_v0.18.1_linux-amd64.tar.gz` linux binary.

In order to make the IPFS node visible to the outside world, certain ports need to opened. By default, the ports are assigned as follows:
- 4001: used for communication between nodes in the IPFS network
- 5001: used for the IPFS HTTP API, which allows you to interact with IPFS using HTTP requests
- 8080: used for the IPFS gateway, which allows you to access IPFS content using a web browser
- 8081: used for the IPFS web UI, which provides a user-friendly interface for interacting with IPFS

In our case it was sufficient to open port **4001** for both sides.
