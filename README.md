<img src="https://www.dkrz.de/@@site-logo/dkrz.svg" alt="drawing" width="200"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/1/18/Ipfs-logo-1024-ice-text.png" alt="drawing" width="100"/>


# IPFS Pinning Service for Open Climate Research Data

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fnfdi4earth%2Fpilotsincubatorlab%2Fincubator%2Fipfs-pinning-service/HEAD)

## Abstract - What is new?
The InterPlanetary File System (IPFS) is a novel decentralized file storage network that allows users to store and share files in a distributed manner, which can make it more resilient if individual infrastructure components fail. It also allows for faster access to content as users can get files directly from other users instead of having to go through a central server. However, one of the challenges of using IPFS is ensuring that the files remain available over time. This is where an IPFS pinning service offers a solution. An IPFS pinning service is a type of service that allows users to store and maintain the availability of their files on the IPFS network. The goal of an IPFS pinning service is to provide a reliable and trusted way for users to ensure that their files remain accessible on the IPFS network. This is accomplished by maintaining a copy of the file on the service's own storage infrastructure, which is then pinned to the IPFS network. This allows users to access the file even if the original source becomes unavailable.

We explored the use of the IPFS for scientific data with a focus on climate data. We set up an IPFS node running on a cloud instance at the German Climate Computing Center where selected scientists can pin their data and make them accessible to the public via the IPFS infrastructure. IPFS is a good choice for climate data, because the open network architecture strengthens open science efforts and enables FAIR data processing workflows. Data within the IPFS is freely accessible to scientists regardless of their location and offers fast access rates to large files. In addition, data within the IPFS is immutable, which ensures that the content of a content identifier does not change over time. Due to the recent development of the IPFS, the project outcomes are novel data science developments for the earth system science and are potentially relevant building blocks to be included in the earth system science community.


## Repository Content
This repositiory contains:
- A project description with general information about the IPFS
- [Installation and implementation details](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/ipfs-pinning-service/-/blob/master/Documents/Installation_and_Implementation.md)
- A [user guide](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/ipfs-pinning-service/-/blob/master/Documents/How_to_Use.md) with an [example notebook](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/ipfs-pinning-service/-/blob/master/how_to_open_ipfs_file.ipynb), which illustrates the use of the service
- Supplementary videos, which explain the project and the use of the service ([science talk](https://youtu.be/scByz8dYzio), [application tutorial](https://youtu.be/jDrBFgUi7Oc))

## How to cite
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7646356.svg)](https://doi.org/10.5281/zenodo.7646356)

## References
Benet, J. (2014). IPFS - content addressed, versioned, P2P file system. CoRR,
abs/1407.3561. http://arxiv.org/abs/1407.3561

## Acknowledgement
This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).
